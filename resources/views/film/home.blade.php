@extends ('layouts.master')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Heroic Features - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('/css/heroic-features.css')}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">LayarKaca 21</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <!-- Jumbotron Header -->
    <header class="jumbotron my-4">
      <h1 class="display-3">Selamat Datang Di LayarKaca 21!</h1>
      <p class="lead">Rasakan menonton yang lebih menarik dari televisi anda di rumah !</p>
      <a href="#" class="btn btn-primary btn-lg">Call to action!</a>
    </header>

    <!-- Page Features -->
    <div class="row text-center">

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card h-100">
          <img class="card-img-top" src="{{asset('images/1615515204.jpg')}}"  alt="image" width="500" height="325">
          <div class="card-body">
            <h4 class="card-title">Doctor Strange</h4>
            <p class="card-text">Dr. Stephen Strange mengalami sebuah kecelakaan yang fatal yang merusak kemampuan motorik kedua tangannya. Demi kesembuhannya, ia mengunjungi seorang penyihir misterius bernama Ancient One di Tibet.</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card h-100">
          <img class="card-img-top" src="{{asset('images/1615513051.jpg')}}"  alt="image" width="500" height="325">
          <div class="card-body">
            <h4 class="card-title">Laskar Pelangi</h4>
            <p class="card-text">Mereka bersekolah dan belajar pada kelas yang sama dari kelas 1 SD sampai kelas 3 SMP, dan menyebut diri mereka sebagai Laskar Pelangi. Pada bagian-bagian akhir cerita, anggota Laskar Pelangi bertambah satu anak perempuan yang bernama Flo, seorang murid pindahan. Keterbatasan yang ada bukan membuat mereka putus asa, tetapi malah membuat mereka terpacu untuk dapat melakukan sesuatu yang lebih baik. </p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card h-100">
          <img class="card-img-top" src="{{asset('images/161458130043345.jpg')}}"  alt="image" width="500" height="325">
          <div class="card-body">
            <h4 class="card-title">Tom and Jerry</h4>
            <p class="card-text">Salah satu adegan dalam Tom and Jerry The Movie mengisahkan tentang Tom dan Jerry yang enggak sengaja bertemu dengan Robyn Starling.

Robin Starling adalah seorang gadis yatim piatu yang melarikan diri dari rumah karena Pristine Figg, bibinya yang jahat.

Robyn bercerita kepada Tom dan Jerry bahwa ibunya telah meninggal dunia sejak dirinya masih bayi.

Sedangkan, ayahnya belum lama juga meninggal dunia karena terkena longsoran pada sebuah ekspedisi pendakian gunung.</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-4">
        <div class="card h-100">
          <img class="card-img-top" src="{{asset('images/161485369150397.jpg')}}"  alt="image" width="500" height="325">
          <div class="card-body">
            <h4 class="card-title">DORAEMON THE MOVIE: Nobita's New Dinosaur</h4>
            <p class="card-text">Nobita menetaskan dua dinosaurus baru, bernama Kyu dan Myu, melalui Time Cloth. Nobita memelihara dan merawat dinosaurus ini, untuk membuktikan kepada teman-temannya bahwa dia dapat menemukan dinosaurus sungguhan. Setelah dinosaurus itu dewasa, Nobita menunjukkannya kepada Shizuka, Gian, dan Suneo. Setelah itu mereka melakukan perjalanan ke masa lalu, di mana mereka dapat melihat rumah Kyu dan Myu, yang dihuni oleh banyak dinosaurus lain. Hal ini malah menyebabkan masalah bagi semua orang.</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
 
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
@endsection
