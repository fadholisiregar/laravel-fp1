@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3"> 
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Jadwal</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/film/jadwal" method="POST" enctype="multipart/form-data">
                  @csrf
                <div class="card-body">
                        <div class="form-group">
                        <label for="film">Pilih Film</label>
                        <select class="form-control" name="film">
                        @foreach($posts as $film)
                            <option value="{{ $film->id }}">{{ $film->judul}}</option>
                        @endforeach
                         </select>
                         </div>
                  <div class="form-group">
                    <label for="body">Tanggal</label>
                    <input type="text" class="form-control" id="desc" name="tanggal" value="{{ old ('tanggal','') }}">
                    @error('tanggal')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>

                 <div class="form-group">
                    <label for="body">Jam</label>
                    <input type="text" class="form-control" id="jam" name="jam" value="{{ old ('jam','') }}">
                    @error('jam')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>

                 <div class="form-group">
                    <label for="body">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" value="{{ old ('harga','') }}">
                    @error('harga')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>

                 <div class="form-group">
                    <label for="body">Jumlah Penonton</label>
                    <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ old ('jumlah','') }}">
                    @error('jumlah')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat Jadwal</button>
                </div>
              </form>
            </div>
</div>

@endsection