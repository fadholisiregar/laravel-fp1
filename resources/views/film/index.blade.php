@extends('layouts.master')

@section('content')
<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Film Post</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                  <div class="alert alert-success">
                      {{session('success')}}
                  </div>
                  @endif
                  <a href="/film/create" class="btn btn-primary mb-2">Tambah Film</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Desc</th>
                      <th>Tahun</th>
                      <th>Gambar</th>
                      <th style="width: 40px">Control</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($posts as $key => $data)
                      <tr>
                      <td>{{ $key + 1}}</td>
                      <td>{{ $data->judul }}</td>
                      <td>{{ $data->desc }}</td>
                      <td>{{ $data->tahun }}</td>
                      <td><img src="{{asset('images/'.$data->gambar)}}" width="500">
                      </td>
                      <td style="display: flex;">
                          <a href="/film/{{$data->id}}" class="btn btn-info btn-sm">show</a> &nbsp;
                          <a href="/film/{{$data->id}}/edit" class="btn btn-primary btn-sm">edit</a> &nbsp;   
                      <form action="/film/{{$data->id}}" method="post">
                      @csrf
                      @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Di Hapus ?')">
                      </form>
                      </td>
                    </tr>
                      @endforeach
                    
                   
                  
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
             
            </div>
</div>
@endsection


@push('scripts1')
<script src="{{asset('sbadmin2/js/swal.min.js')}}"></script>
<script>
    Swal.fire({
        title: "Welcome",
        text: "Admin FILM SANBER",
        icon: "success",
        confirmButtonText: "OK",
    });
</script>
@endpush