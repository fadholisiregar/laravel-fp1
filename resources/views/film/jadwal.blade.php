@extends('layouts.master')

@section('content')
<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Jadwal Film</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                  <div class="alert alert-success">
                      {{session('success')}}
                  </div>
                  @endif
                  <a href="/film/createjadwal" class="btn btn-primary mb-2">Tambah Jadwal Film</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Tanggal Tayang</th>
                      <th>Jam Tayang</th>
                      <th>Harga</th>
                      <th>Jumlah Penonton</th>
                      <th style="width: 40px">Control</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($posts as $key => $data)
                      <tr>
                      <td>{{ $key + 1}}</td>
                      <td>{{ $data->judul }}</td>
                      <td>{{ $data->tanggal_tayang }}</td>
                      <td>{{ $data->jam_tayang }}</td>
                      <td>{{ $data->harga }}</td>
                      <td>{{ $data->jumlah_penonton }}</td>
                      
                      <td style="display: flex;">
                          <a href="/film/{{$data->id}}/edit" class="btn btn-primary btn-sm">edit</a> &nbsp;   
                      <form action="/film/{{$data->id}}" method="post">
                      @csrf
                      @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Di Hapus ?')">
                      </form>
                      </td>
                    </tr>
                      @endforeach
                    
                   
                  
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
             
            </div>
</div>

@endsection