@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3"> 
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/film/{{$post->id}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" id="judul" class="form-control" name='judul' placeholder="Enter title" value="{{ old ('judul',$post->judul) }}">
                  @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Desc</label>
                    <input type="text" class="form-control" id="desc" name="desc"  value="{{ old ('desc',$post->desc) }}">
                    @error('desc')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>

                 <div class="form-group">
                    <label for="body">Tahun</label>
                    <input type="text" class="form-control" id="tahun" name="tahun"  value="{{ old ('tahun',$post->tahun) }}">
                    @error('tahun')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>

                 <div class="form-group">
                    <label for="file">Gambar</label>
                    <input type="file" class="form-control" id="file" name="file">
                    @error('gambar')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                 </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>

@endsection