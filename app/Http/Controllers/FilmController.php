<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;

class FilmController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $posts = DB::table('film')->get();
        //dd($posts);
        return view('film.index',compact('posts'));
    }

    public function jadwal(){
        $posts = DB::table('jadwal')
            ->join('film', 'jadwal.id_film', '=', 'film.id')
            ->select('jadwal.*', 'film.judul')
            ->get();
        //dd($posts);
        return view('film.jadwal',compact('posts'));
    }

    public function create(){
        return view('film.create');
    }

    public function createjadwal(){
        $posts = DB::table('film')->get();
        return view('film.createjadwal',compact('posts'));
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:film',
            'desc' => 'required',
            'tahun' => 'required',
            'file' => 'required|image|mimes:jpeg,jpg,png'
            
        ]);
            $gambar=$request->file;
            $namafile=time().'.'.$gambar->getClientOriginalExtension();
            $gambar->move('images/',$namafile);

        $query = DB::table('film')->insert([
            "judul" => $request["judul"],
            "desc" => $request["desc"],
            "tahun" => $request["tahun"],
            "gambar" => $namafile
        ]);

        return redirect('/film')->with('success','Post Berhasil Disimpan');
    }

    public function storejadwal(Request $request){
        //dd($request->all());
        $request->validate([
            'film' => 'required',
            'tanggal' => 'required',
            'jam' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
            
        ]);
            
        $query = DB::table('jadwal')->insert([
            "id_film" => $request["film"],
            "tanggal_tayang" => $request["tanggal"],
            "jam_tayang" => $request["jam"],
            "harga" => $request["harga"],
            "jumlah_penonton" => $request["jumlah"]
        ]);

        return redirect('/film/createjadwal')->with('success','Post Berhasil Disimpan');
    }

    public function show($id){
        $post = DB::table('film')-> where('id',$id)->first();
        //dd($post);
        return view ('film.show',compact('post'));
    }

    public function edit($id){
        $post = DB::table('film')-> where('id',$id)->first();
        //dd($post);
        return view ('film.edit',compact('post'));
    }

    public function update($id,Request $request){
        $request->validate([
            'judul' => 'required',
            'desc' => 'required',
            'tahun' => 'required',
            'file' => 'required|image|mimes:jpeg,jpg,png'

        ]);
        
            $gambar=$request->file;
            $namafile=time().'.'.$gambar->getClientOriginalExtension();
            $gambar->move('images/',$namafile);

        $query = DB::table('film')
                     ->where('id',$id)
                     ->update([
                        'judul' => $request['judul'],
                        'desc' => $request['desc'],
                        'tahun' => $request['tahun'],
                        'gambar' => $namafile
                        
                     ]);
        return redirect('/film')->with('success','Berhasil Update Post');
    }

    public function destroy($id){
        $query = DB::table('film')-> where('id',$id)->delete();
        return redirect('/film')->with('success','Post Berhasil di delete');

    }

 

}
